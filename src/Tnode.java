import java.util.*;

public class Tnode {

   private String name;
   private Tnode firstChild;
   private Tnode nextSibling;

   @Override
   public String toString() {
      StringBuffer b = new StringBuffer();
      if (hasChildren()) {
         b.append(getName())
                 .append("(")
                 .append(getFirstChild().toString())
                 .append(",")
                 .append(getNextSibling().toString())
                 .append(")");
      } else {
         b.append(getName());
      }
      return b.toString();
   }

   private void setFirstChild(Tnode firstChild) {
      this.firstChild = firstChild;
   }

   private void setNextSibling(Tnode nextSibling) {
      this.nextSibling = nextSibling;
   }

   private void setName(String name) {
      this.name = name;
   }

   private Tnode getFirstChild() {
      return this.firstChild;
   }

   private Tnode getNextSibling() {
      return this.nextSibling;
   }

   private String getName() {
      return this.name;
   }


   private boolean hasChildren() {
      try {
         return !this.getFirstChild().toString().equals("");
      } catch (NullPointerException e) {
         return false;
      }
   }

   public static Tnode buildFromRPN (String pol) {

      StringBuffer errorMessage = new StringBuffer();

      try {


      if (pol.isEmpty()) {
         errorMessage.append("RPN can't be empty");
         throw new RuntimeException();
      }

      Tnode root = null;
      LinkedList<Tnode> nodeStack = new LinkedList<>();


         for (String s : pol.split("\\s+")) {

            Tnode tNode = new Tnode();

            if (knownOperator(s)) {

               try {

                  if (s.equals("DUP")) {
                     Tnode onlyChild = nodeStack.pop();
                     nodeStack.push(onlyChild);
                     nodeStack.push(onlyChild);
                     continue;
                  }

                  Tnode nextSibling = nodeStack.pop();
                  Tnode firstChild = nodeStack.pop();

                  if (s.equals("SWAP")) {
                     nodeStack.push(nextSibling);
                     nodeStack.push(firstChild);
                  } else if (s.equals("ROT")) {
                     Tnode thirdNode = nodeStack.pop();
                     nodeStack.push(firstChild);
                     nodeStack.push(nextSibling);
                     nodeStack.push(thirdNode);
                  } else {
                     tNode.setNextSibling(nextSibling);
                     tNode.setFirstChild(firstChild);
                     tNode.setName(s);
                     nodeStack.push(tNode);
                     root = tNode;
                  }
               } catch (Exception e) {
                  errorMessage.append("Too few numbers or misplaced operator in RPN! Operator: ")
                          .append(s);
                  throw new RuntimeException();
               }
            } else {
               try {
                  double isViableNumber = Double.parseDouble(s);
                  tNode.setName(s);
                  nodeStack.push(tNode);
                  root = tNode;
               } catch (Exception e) {
                  errorMessage.append("Unknown symbol! Symbol: ")
                          .append(s);
                  throw new RuntimeException();
               }
            }
         }
         if (nodeStack.size() > 1) {
            errorMessage.append("Too many numbers in operation.");
            throw new RuntimeException();
         }
         return root;

      } catch (Exception e) {
         errorMessage.append(" RPN: ").append(pol);
         throw new RuntimeException(errorMessage.toString());
      }



   }

   public static void main (String[] param) {
      String rpn = "1 2 +";
      System.out.println ("RPN: " + rpn);
      Tnode res = buildFromRPN (rpn);
      System.out.println ("Tree: " + res);
   }

   private static boolean knownOperator(String operator) {
      return  (operator.equals("+") | operator.equals("-") | operator.equals("*") | operator.equals("/") |
              operator.equals("SWAP") | operator.equals("DUP") | operator.equals("ROT"));
   }
}

